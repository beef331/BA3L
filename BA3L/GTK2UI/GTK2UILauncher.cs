﻿using Gtk;

namespace BA3L
{
	/// <summary>
	/// Work around the possibility that GTK# isn't real
	/// </summary>
	public class GTK2UILauncher
	{
		Core core;

		public GTK2UILauncher(Core core)
		{
			this.core = core;
		}

		public void Run()
		{
			Application.Init();
			MainWindow win = new MainWindow(core);
			win.Show();
			Application.Run();
		}
	}
}
