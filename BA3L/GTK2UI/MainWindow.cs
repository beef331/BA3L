﻿using System;
using System.Collections.Generic;
using BA3L;
using Gtk;

public partial class MainWindow : Gtk.Window
{
	Core core;
	ListStore tvModListStore;

	public MainWindow(Core core) : base(Gtk.WindowType.Toplevel)
	{
		Build();
		this.core = core;

		UpdateParameterValues();
		SetupModList();
		UpdateModList();
		core.ModSystem.RefreshModsEvent += OnUpdateModList;

		hboxProtonExe.Visible &= Utilities.IsProton(); // Hide Proton executable setting if we aren't using it.
		UpdateSettingValues();
	}

	protected void OnDeleteEvent(object sender, DeleteEventArgs a)
	{
		Application.Quit();
		a.RetVal = true;
	}

	protected void Play(Core.PlayOptions playOptions)
	{
		try
		{
			core.Play(playOptions);
		}
		catch(BA3L.Exceptions.ModNotReadyException ex)
		{
			string message = "One or more mods are not ready.\nFirst failed mod: " + ex.FailedMod.Name;
			MessageDialog md = new MessageDialog(this, DialogFlags.DestroyWithParent,
				MessageType.Error, ButtonsType.Ok, message);
			md.WindowPosition = WindowPosition.Center;
			md.Run();
			md.Destroy();
		}
		catch(BA3L.Exceptions.ProtonNotFoundException ex)
		{
			string message = "Proton not found: " + ex.Message
				+ "\nSet the path to your Proton executable in Settings.";
			MessageDialog md = new MessageDialog(this, DialogFlags.DestroyWithParent,
				MessageType.Error, ButtonsType.Ok, message);
			md.WindowPosition = WindowPosition.Center;
			md.Run();
			md.Destroy();
		}
	}

	protected void OnBtnPlayWithModsClicked(object sender, EventArgs e)
	{
		Core.PlayOptions playOptions = new Core.PlayOptions();
		playOptions.IncludeLaunchOptions = true;
		playOptions.PlayWithMods = true;
		Play(playOptions);
	}

	protected void OnBtnPlayWithoutModsClicked(object sender, EventArgs e)
	{
		Core.PlayOptions playOptions = new Core.PlayOptions();
		playOptions.IncludeLaunchOptions = true;
		playOptions.PlayWithMods = false;
		Play(playOptions);
	}

	protected void OnNotebook1SwitchPage(object o, SwitchPageArgs args)
	{
		Widget w = notebook1.GetNthPage((int)args.PageNum);
		if(w.Name == "vboxParameters")
		{
			UpdateParameterValues();
		}
		else if(w.Name == "swModList")
		{
			Console.WriteLine("Mod list selected");
		}
	}

	private void SetupModList()
	{
		// Columns:
		// Mod Enabled, Mod Name, Mod Identifier
		TreeViewColumn enabledColumn = new TreeViewColumn();
		enabledColumn.Title = "Enabled";
		CellRendererToggle enableCell = new CellRendererToggle();
		enableCell.Toggled += ModEnableToggled;
		enabledColumn.PackStart(enableCell, true);

		TreeViewColumn modNameColumn = new TreeViewColumn();
		modNameColumn.Title = "Mod name";
		CellRendererText nameCell = new CellRendererText();
		modNameColumn.PackStart(nameCell, true);

		TreeViewColumn identifierColumn = new TreeViewColumn();
		identifierColumn.Title = "Identifier";
		CellRendererText identifierCell = new CellRendererText();
		identifierColumn.PackStart(identifierCell, true);

		// Cell data funcs
		enabledColumn.SetCellDataFunc(enableCell, new TreeCellDataFunc(RenderModEnabled));
		modNameColumn.SetCellDataFunc(nameCell, new TreeCellDataFunc(RenderModName));
		identifierColumn.SetCellDataFunc(identifierCell, new TreeCellDataFunc(RenderModIdentifier));

		ListStore store = new ListStore(typeof(Mod));

		tvModList.Model = store;
		tvModListStore = store; // Do we "need" to store this if we can get it from tvModList.Model?

		tvModList.AppendColumn(enabledColumn);
		tvModList.AppendColumn(modNameColumn);
		tvModList.AppendColumn(identifierColumn);
	}

	private void RenderModEnabled(TreeViewColumn column, CellRenderer cell, TreeModel model, TreeIter iter)
	{
		Mod mod = (Mod)tvModListStore.GetValue(iter, 0);
		(cell as CellRendererToggle).Active = mod.Enabled;
	}

	private void RenderModName(TreeViewColumn column, CellRenderer cell, TreeModel model, TreeIter iter)
	{
		Mod mod = (Mod)tvModListStore.GetValue(iter, 0);
		(cell as CellRendererText).Text = mod.Name;
	}

	private void RenderModIdentifier(TreeViewColumn column, CellRenderer cell, TreeModel model, TreeIter iter)
	{
		Mod mod = (Mod)tvModListStore.GetValue(iter, 0);
		(cell as CellRendererText).Text = mod.Identifier;
	}

	private void ModEnableToggled(object o, ToggledArgs args)
	{
		TreeIter iter;

		if(tvModListStore.GetIter(out iter, new TreePath(args.Path)))
		{
			var toggledMod = (Mod)tvModListStore.GetValue(iter, 0);
			core.ModSystem.ToggleMod(toggledMod);
		}
	}

	private void UpdateModList()
	{
		tvModListStore.Clear();

		List<Mod> sortedMods = new List<Mod>(core.ModSystem.GetMods());
		sortedMods.Sort(new ModNameComparer<Mod>());

		foreach(Mod mod in sortedMods)
		{
			tvModListStore.AppendValues(mod);
		}
	}

	private void OnUpdateModList(object sender, RefreshModsEventArgs e)
	{
		UpdateModList();
	}

	private void UpdateParameterValues()
	{
		// TODO more parameters
		btnNoSplash.Active = BA3L.Settings.NoSplash;
		btnSkipIntro.Active = BA3L.Settings.SkipIntro;
		btnWindowed.Active = BA3L.Settings.Windowed;
	}

	private void UpdateSettingValues()
	{
		// Play tab settings (BattlEye)
		btnPlayBattlEye.Active = BA3L.Settings.PlayWithBattlEye;

		// Settings tab
		fcProtonExe.SetFilename(BA3L.Settings.ProtonPath);
	}

	protected void OnBtnNoSplashToggled(object sender, EventArgs e)
	{
		// "Toggled" seems to be called when their value is changed
		// in UpdateParameterValues.
		BA3L.Settings.NoSplash = btnNoSplash.Active;
	}

	protected void OnBtnSkipIntroToggled(object sender, EventArgs e)
	{
		BA3L.Settings.SkipIntro = btnSkipIntro.Active;
	}

	protected void OnBtnWindowedToggled(object sender, EventArgs e)
	{
		BA3L.Settings.Windowed = btnWindowed.Active;
	}

	protected void OnBtnPlayBattlEyeToggled(object sender, EventArgs e)
	{
		BA3L.Settings.PlayWithBattlEye = btnPlayBattlEye.Active;
	}

	protected void OnBtnSaveModPresetClicked(object sender, EventArgs e)
	{
		// Open file chooser to save
		FileChooserDialog fc =
			new FileChooserDialog(
				"Save Preset",
				this,
				FileChooserAction.Save,
				"Cancel", ResponseType.Cancel,
				"Save", ResponseType.Accept
			);

		fc.Filter = new FileFilter();
		fc.Filter.AddPattern("*" + BA3L.Settings.EXT_BA3LSAVE);
		fc.SetCurrentFolder(BA3L.Settings.GetLauncherSaveDirectory());
		fc.CurrentName = "preset" + BA3L.Settings.EXT_BA3LSAVE;
		if(fc.Run() == (int)ResponseType.Accept)
		{
			BA3L.Settings.SaveModPresetFile(fc.Filename, core.ModSystem.GetEnabledMods());
		}

		fc.Destroy();
	}

	protected void OnBtnLoadModPresetClicked(object sender, EventArgs e)
	{
		// Open file chooser to load
		FileChooserDialog fc =
			new FileChooserDialog(
				"Load preset",
				this,
				FileChooserAction.Open,
				"Cancel", ResponseType.Cancel,
				"Load", ResponseType.Accept
			);

		fc.Filter = new FileFilter();
		fc.Filter.AddPattern("*" + BA3L.Settings.EXT_BA3LSAVE);
		fc.SetCurrentFolder(BA3L.Settings.GetLauncherSaveDirectory());
		if(fc.Run() == (int)ResponseType.Accept)
		{
			// Load file
			ModPresetStruct modPreset = BA3L.Settings.LoadModPresetFile(fc.Filename);
			// Probably isn't really the UI's responsibility to call this part.
			core.ModSystem.LoadModPreset(modPreset);

			// Force UI to update now
			tvModList.QueueDraw();
		}

		fc.Destroy();
	}

	protected void OnFcProtonExeSelectionChanged(object sender, EventArgs e)
	{
		BA3L.Settings.ProtonPath = fcProtonExe.Filename;
	}
}
