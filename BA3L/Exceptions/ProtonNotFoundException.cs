﻿using System;
namespace BA3L.Exceptions
{
	public class ProtonNotFoundException: Exception
	{
		public ProtonNotFoundException() {}
		public ProtonNotFoundException(string message) : base(message) {}
	}
}
