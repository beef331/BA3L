﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;


namespace BA3L
{
	public class Mod
	{
		public ulong WorkshopId { get; private set; }   // Identifier for workshop enable/disable
		private Steamworks.Ugc.Item workshopItem;

		private string ModName = "";

		// Name of last folder, identifier for enable/disable
		public string ModFolderName
		{
			get
			{
				if(UsesSteamWorkshop())
				{
					return WorkshopId.ToString();
				}
				else
				{
					return new DirectoryInfo(nonWorkshopFolderPath).Name;
				}
			}
		}

		public bool Enabled { get; private set; }

		public string Identifier
		{
			get
			{
				if(UsesSteamWorkshop())
				{
					return WorkshopId.ToString();
				}
				else
				{
					return ModFolderName;
				}
			}
		}

		public string Name
		{
			get
			{
				if(ModName != "")
					return ModName;
				else return ModFolderName;
			}
		}

		// Workshop state flags
		public bool IsInstalled => workshopItem.IsInstalled;
		public bool IsDownloading => workshopItem.IsDownloading;
		public bool IsDownloadPending => workshopItem.IsDownloadPending;
		public bool IsSubscribed => workshopItem.IsSubscribed;
		public bool NeedsUpdate => workshopItem.NeedsUpdate;
		public bool IsReadyToPlay => !UsesSteamWorkshop() || (IsInstalled && !IsDownloading && !IsDownloadPending && !NeedsUpdate);


        private readonly string nonWorkshopFolderPath;

		public Mod(string modDirectory)
		{
			// Assume modDirectory is full path
			nonWorkshopFolderPath = modDirectory;

			// TODO look for mod name in meta.cpp if we don't find it in mod.cpp

			string modCppPath = Path.Combine(nonWorkshopFolderPath, "mod.cpp");
			if(File.Exists(modCppPath))
			{
				List<KeyValuePair<string, string>> keyValuePairs = ReadModCpp(modCppPath);

				KeyValuePair<string, string> namePair = FindKeyValuePair("name", keyValuePairs);
				if(!namePair.Equals(default(KeyValuePair<string, string>)))
				{
					ModName = namePair.Value;
				}
			}
		}

		public Mod(Steamworks.Ugc.Item item)
		{
			workshopItem = item;
			WorkshopId = workshopItem.Id;
			ModName = workshopItem.Title;
		}

		public List<KeyValuePair<string, string>> ReadModCpp(string modCppPath)
		{
			List<KeyValuePair<string, string>> keyValuePairs = new List<KeyValuePair<string, string>>();
			// Assume file exists
			string modCppData = File.ReadAllText(modCppPath);

			// Cover both Windows and Unix line ending forms.
			char[] separators = { '\r', '\n' };
			string[] lines = modCppData.Split(separators, StringSplitOptions.RemoveEmptyEntries);

			// Key, optional whitespace, =, optional whitespace, value, semicolon
			Regex modRegex = new Regex(@"(?<key>\w+)\s*=\s*(?<value>([^;]*))");

			foreach(string line in lines)
			{
				Match keyValueMatch = modRegex.Match(line);
				if(!keyValueMatch.Success)
					continue;

				string key = keyValueMatch.Groups["key"].Value;
				string value = keyValueMatch.Groups["value"].Value;
				value = value.Trim('"');
				KeyValuePair<string, string> pair = new KeyValuePair<string, string>(key, value);
				keyValuePairs.Add(pair);
			}

			return keyValuePairs;
		}

		public KeyValuePair<string, string> FindKeyValuePair(string keyName, List<KeyValuePair<string, string>> keyValuePairs)
		{
			foreach(KeyValuePair<string, string> pair in keyValuePairs)
			{
				if(pair.Key == keyName)
				{
					return pair;
				}
			}

			return default(KeyValuePair<string, string>);
		}

		// Some of these could be made into properties.
		public bool UsesSteamWorkshop()
		{
			return WorkshopId > 0L;
		}

		public string GetPath()
		{
			if(UsesSteamWorkshop())
			{
				if(IsInstalled)
				{
					return workshopItem.Directory;
				}
				else
				{
					return null;
				}
			}
			else
			{
				return nonWorkshopFolderPath;
			}
		}

		/// <summary>
		/// Gets the path of game relative to Wine/Proton's "Z:\" drive.
		/// </summary>
		/// <returns>The mod's path as if it were running through Proton.</returns>
		public string GetProtonPath()
		{
			const string protonDisk = "Z:";
			return protonDisk + GetPath().Replace("/", "\\\\"); // Apparently
		}

		/// <summary>
		/// Set this mod as enabled. Only call from ModManager.EnableMod().
		/// </summary>
		public void Enable()
		{
			Enabled = true;
		}

		/// <summary>
		/// Set this mod as disabled. Only call from ModManager.DisableMod().
		/// </summary>
		public void Disable()
		{
			Enabled = false;
		}
	}
}
