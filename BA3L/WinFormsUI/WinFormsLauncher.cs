using System.Windows.Forms;

namespace BA3L.WinFormsUI
{
    public static class WinFormsLauncher
    {
        public static void Run(Core core)
        {
			Application.EnableVisualStyles();
			Application.Run(new MainForm(core));
			// WinForms has a tendency to crash on Mono
			// But it crashes the runtime itself, catching here wouldn't do anything.
		}
    }
}