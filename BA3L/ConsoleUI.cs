﻿using System;
using System.Runtime.InteropServices;

namespace BA3L
{
	public class ConsoleUI
	{
		private readonly Core core;

		public ConsoleUI(Core core)
		{
			this.core = core;
		}

		private enum Command
		{
			CMD_NONE,
			CMD_PLAY,
			CMD_EXIT,
			CMD_PARAMETER,
			CMD_LISTINSTALLEDMODS,
			CMD_TESTING,
			CMD_ENABLELOCALMOD,
			CMD_ENABLEWORKSHOPMOD,
			CMD_DISABLELOCALMOD,
			CMD_DISABLEWORKSHOPMOD,
			CMD_LISTACTIVEMODS,
			CMD_SHOWMODPARAMETERS,
			CMD_LOADPRESET,
			CMD_SAVEPRESET,
			CMD_SETPROTONEXE
		}

		public bool Run()
		{
			Console.WriteLine("Console UI Started");
			if(!EnsureConsoleWindow())
			{
				return false;
			}

			//Loop and input commands, executing until exit
			bool exit = false;
			while(!exit)
			{
				//Get command and interpret it
				string input = "";
				Command c = GetCommand(out input);
				switch(c)
				{
					case Command.CMD_PLAY:
						exit = Play(input);
						break;
					case Command.CMD_EXIT:
						exit = true;
						break;
					case Command.CMD_PARAMETER:
						SetParameter(input);
						break;
					case Command.CMD_LISTINSTALLEDMODS:
						ListInstalledMods();
						break;
					case Command.CMD_TESTING: // Simple testing commands handled in GetCommand()
						break;
					case Command.CMD_ENABLELOCALMOD:
						EnableLocalMod(input);
						break;
					case Command.CMD_DISABLELOCALMOD:
						DisableLocalMod(input);
						break;
					case Command.CMD_ENABLEWORKSHOPMOD:
						EnableWorkshopMod(input);
						break;
					case Command.CMD_DISABLEWORKSHOPMOD:
						DisableWorkshopMod(input);
						break;
					case Command.CMD_LISTACTIVEMODS:
						ListActiveMods();
						break;
					case Command.CMD_SHOWMODPARAMETERS:
						ShowModParameters();
						break;
					case Command.CMD_LOADPRESET:
						LoadPreset();
						break;
					case Command.CMD_SAVEPRESET:
						SavePreset();
						break;
					case Command.CMD_SETPROTONEXE:
						SetProtonExe();
						break;
					default:
						Console.WriteLine("Command not recognized.");
						break;
				}
			}

			return true;
		}

		private Command GetCommand(out string input)
		{
			Console.Write("Enter command: ");
			input = Console.ReadLine();
			string inputLower = input.ToLower();

			//Parse command
			if(input.Length > 0)
			{
				if(inputLower.StartsWith("play", StringComparison.Ordinal))
				{
					return Command.CMD_PLAY;
				}
				else if(inputLower == "exit")
				{
					return Command.CMD_EXIT;
				}
				else if(inputLower.StartsWith("parameter", StringComparison.Ordinal))
				{
					return Command.CMD_PARAMETER;
				}
				else if(inputLower == "list installed mods")
				{
					return Command.CMD_LISTINSTALLEDMODS;
				}
				else if(inputLower == "isproton")
				{
					Console.WriteLine("Using Proton: " + Utilities.IsProton());
					return Command.CMD_TESTING;
				}
				else if(inputLower.StartsWith("enable local mod", StringComparison.Ordinal))
				{
					return Command.CMD_ENABLELOCALMOD;
				}
				else if(inputLower.StartsWith("disable local mod", StringComparison.Ordinal))
				{
					return Command.CMD_DISABLELOCALMOD;
				}
				else if(inputLower.StartsWith("enable workshop mod", StringComparison.Ordinal))
				{
					return Command.CMD_ENABLEWORKSHOPMOD;
				}
				else if(inputLower.StartsWith("disable workshop mod", StringComparison.Ordinal))
				{
					return Command.CMD_DISABLEWORKSHOPMOD;
				}
				else if(inputLower == "list active mods" || inputLower == "list enabled mods")
				{
					return Command.CMD_LISTACTIVEMODS;
				}
				else if(inputLower == "show mod parameters")
				{
					return Command.CMD_SHOWMODPARAMETERS;
				}
				else if(inputLower == "loadpreset")
				{
					return Command.CMD_LOADPRESET;
				}
				else if(inputLower == "savepreset")
				{
					return Command.CMD_SAVEPRESET;
				}
				else if(inputLower.StartsWith("set proton exe", StringComparison.Ordinal))
				{
					return Command.CMD_SETPROTONEXE;
				}

			}

			// If we don't find a matching command
			return Command.CMD_NONE;
		}


		private bool EnsureConsoleWindow()
		{
			if(Utilities.GetOS() == Utilities.OS.OS_WINDOWS)
			{
				if(GetConsoleWindow() != IntPtr.Zero)
				{
					return true;
				}
				else
				{
					Console.WriteLine("Does not have console");
					return false;
				}
			}

			//Idk how to tell on OSX/Linux so guess true
			return true;
		}

		[DllImport("kernel32.dll")]
		static extern IntPtr GetConsoleWindow();

		private bool Play(string input)
		{
			try
			{
				bool result = core.Play();

				if(result)
				{
					Console.WriteLine("Arma 3 started.");
				}
				else
				{
					Console.WriteLine("Arma 3 not successfully started.");
				}

				return input.EndsWith("exit", StringComparison.Ordinal);
			}
			catch(Exceptions.ModNotReadyException e)
			{
				Console.WriteLine("One or more mods were not ready to launch.");
				if(e.FailedMod != null)
				{
					Console.WriteLine("First failed mod: " + e.FailedMod.Name);
				}

				return false;
			}
			catch(Exceptions.ProtonNotFoundException e)
			{
				Console.WriteLine("PlayProton error: " + e.Message);
				return false;
			}
		}

		private void SetParameter(string input)
		{
			if (input.Length < 0)
				return;

			string[] splitInput = input.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

			if(splitInput.Length <= 1)
			{
				Console.WriteLine("Error: No arguments given.");
				Console.WriteLine("Usage: paramater paramname [value]");
				return;
			}

			if (splitInput.Length >= 2)
			{

				if (splitInput.Length == 2)
				{
					// e.g. parameter paramname
					// Just display the value of the parameter
					string parameterToCheck = splitInput[1].ToLower();
					if(parameterToCheck == Settings.KEY_NOSPLASH.ToLower())
					{
						Console.WriteLine(Settings.KEY_NOSPLASH + " is " + Settings.NoSplash);
					}
					else if(parameterToCheck == Settings.KEY_SKIPINTRO.ToLower())
					{
						Console.WriteLine(Settings.KEY_SKIPINTRO + " is " + Settings.SkipIntro);
					}
					else if(parameterToCheck == Settings.KEY_WINDOWED.ToLower())
					{
						Console.WriteLine(Settings.KEY_WINDOWED + " is " + Settings.Windowed);
					}

				}
				else if (splitInput.Length >= 3)
				{
					// e.g paramater paramname value
					// Change its value
					string parameterToSet = splitInput[1].ToLower();
					try
					{
						if(parameterToSet == Settings.KEY_NOSPLASH.ToLower())
						{
							Settings.NoSplash = StrToBool(splitInput[2]);
						}
						else if(parameterToSet == Settings.KEY_SKIPINTRO.ToLower())
						{
							Settings.SkipIntro = StrToBool(splitInput[2]);
						}
						else if(parameterToSet == Settings.KEY_WINDOWED.ToLower())
						{
							Settings.SkipIntro = StrToBool(splitInput[2]);
						}
					}
					catch(ArgumentException e)
					{
						Console.WriteLine(e.Message);
					}
				}
			}
		}

		private bool StrToBool(string input)
		{
			string inputLower = input.ToLower();
			if(inputLower == "true")
			{
				return true;
			}
			else if(inputLower == "false")
			{
				return false;
			}
			else
			{
				throw new ArgumentException("Error: " + input + " does not resolve to a boolean!");
			}
		}

		private void ListInstalledMods()
		{
			var mods = core.ModSystem.GetMods();

			foreach(Mod mod in mods)
			{
				// Reasonable defaults for the string formatting.
				Console.WriteLine("| Name: {0, -55} | Identifier: {1, -25} | Ready: {2, -6} |", 
					mod.Name, mod.Identifier, mod.IsReadyToPlay.ToString());
			}
		}

		private void EnableLocalMod(string input)
		{
			// Trim to find identifier
			// Send to mod manager
			int index = "enable local mod".Length;
			string modIdentifier = input.Substring(index).TrimStart(' ');
			Console.WriteLine("Enabling mod: " + modIdentifier);
			bool result = core.ModSystem.EnableModByFolderName(modIdentifier);
			Console.WriteLine("Mod enabled: " + result);
		}

		private void DisableLocalMod(string input)
		{
			int index = "disable local mod".Length;
			string modIdentifier = input.Substring(index).TrimStart(' ');
			Console.WriteLine("Disabling mod: " + modIdentifier);
			core.ModSystem.DisableModByFolderName(modIdentifier);
		}

		private void EnableWorkshopMod(string input)
		{
			int index = "enable workshop mod".Length;
			string modIdentifierString = input.Substring(index).TrimStart(' ');
			ulong modIdentifier;
			bool success = UInt64.TryParse(modIdentifierString, out modIdentifier);
			if(success)
			{
				Console.WriteLine("Enabling workshop mod: " + modIdentifierString);
				core.ModSystem.EnableModByWorkshopId(modIdentifier);
			}
			else
			{
				Console.WriteLine("Failed to parse mod identifier: " + modIdentifierString);
			}
		}

		private void DisableWorkshopMod(string input)
		{
			int index = "disable workshop mod".Length;
			string modIdentifierString = input.Substring(index).TrimStart(' ');
			ulong modIdentifier;
			bool success = UInt64.TryParse(modIdentifierString, out modIdentifier);
			if(success)
			{
				Console.WriteLine("Disabling workshop mod: " + modIdentifierString);
				core.ModSystem.DisableModByWorkshopId(modIdentifier);
			}
			else
			{
				Console.WriteLine("Failed to parse mod identifier: " + modIdentifierString);
			}
		}

		private void ListActiveMods()
		{
			var activeMods = core.ModSystem.GetEnabledMods();

			foreach(Mod mod in activeMods)
			{
				// Reasonable defaults for the string formatting.
				Console.WriteLine("| Name: {0, -55} | Identifier: {1, -25} | Ready: {2, -6} |",
					mod.Name, mod.Identifier, mod.IsReadyToPlay.ToString());
			}
		}

		private void ShowModParameters()
		{
			try
			{
				Console.WriteLine(core.ModSystem.GenerateModLaunchParameters());
			}
			catch(Exceptions.ModNotReadyException e)
			{
				Console.WriteLine("One or more mods were not ready to generate launch parameters.");
				if(e.FailedMod != null)
				{
					Console.WriteLine("First failed mod: " + e.FailedMod.Name);
				}
			}
		}

		private void SavePreset()
		{
			Console.Write("Enter save name: ");
			string saveName = Console.ReadLine();
			string savePath = System.IO.Path.Combine(Settings.GetLauncherSaveDirectory(), saveName);
			Console.WriteLine("Saving to path: " + @savePath);
			Settings.SaveModPresetFile(savePath, core.ModSystem.GetEnabledMods());
		}

		private void LoadPreset()
		{
			Console.Write("Enter load name: ");
			string loadName = Console.ReadLine();
			if(!loadName.EndsWith(".ba3lsave", StringComparison.Ordinal))
			{
				loadName += ".ba3lsave";
			}
			string loadPath = System.IO.Path.Combine(Settings.GetLauncherSaveDirectory(), loadName);
			Console.WriteLine("Loading from path: " + @loadPath);

			try
			{
				ModPresetStruct modPreset = Settings.LoadModPresetFile(loadPath);
				core.ModSystem.LoadModPreset(modPreset);
			}
			catch(System.IO.FileNotFoundException e)
			{
				Console.WriteLine(e.Message);
			}

		}

		private void SetProtonExe()
		{
			Console.Write("Enter path to Proton executable: ");
			string executablepath = Console.ReadLine();
			Console.WriteLine("Path was: " + executablepath);

			if(System.IO.File.Exists(@executablepath))
			{
				Settings.ProtonPath = @executablepath;
				Console.WriteLine("Path set.");
			}
			else
			{
				Console.WriteLine("Proton executable not found.");
			}
		}
	}
}