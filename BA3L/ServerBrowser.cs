using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Steamworks.ServerList;
using Steamworks.Data;

namespace BA3L
{
    //Should be static class since instantiation is redundant, but keeping with current conventions
    public class ServerBrowser
    {
        private List<ServerInfo> servers;
        public Dictionary<ServerInfo, Dictionary<String, string>> serverRules;
		public static Action<List<ServerInfo>> OnServersPolled;

        public async void GetServers(List<(string,string)> filter = null)
        {
            using (var list = new Internet())
            {
                if (filter != null)
                {
                    foreach ((string,string) tuple in filter)
                    {
                        list.AddFilter(tuple.Item1, tuple.Item2);
                    }
                }
                await list.RunQueryAsync(300);
                servers = list.Responsive;
            }
            OnServersPolled?.Invoke(servers);
        }

        public async void GetRules(ServerInfo server)
        {
            Dictionary<string, string> rules = await server.QueryRulesAsync();
            lock (serverRules)
            {
                if (serverRules.ContainsKey(server))
                {
                    serverRules[server] = rules;
                }
                else
                {
                    serverRules.Add(server, rules);
                }
            }
        }


    }
}