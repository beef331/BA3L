﻿using System;
using System.Collections.Generic;

namespace BA3L
{
	/// <summary>
	/// Compares mod names
	/// I needed it for a linq query I used to sort mods in the GUIs
	/// There is probably a better way to do this within the GUI itself.
	/// Obviously is going to crash if <typeparamref name="T"/> is a type other than Mod
	/// </summary>
	public class ModNameComparer<T> : IComparer<T>
	{
		public int Compare(T x, T y)
		{
			return new System.Collections.CaseInsensitiveComparer().Compare((x as Mod).Name, (y as Mod).Name);
		}
	}
}
