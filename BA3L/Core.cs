﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using Steamworks.Data;

namespace BA3L
{
    public class Core
    {
        readonly List<string> gameArgs;
        readonly Dictionary<string, bool> launcherArgs;

        public ModManager ModSystem { get; private set; }
        public ServerBrowser ServerBrowser { get; private set; }

        const string EXECUTABLE_32BIT = "arma3.exe";
        const string EXECUTABLE_64BIT = "arma3_x64.exe";
        const string EXECUTABLE_BATTLEYE = "arma3battleye.exe";

        public Core(List<string> gameArgs, Dictionary<string, bool> launcherArgs)
        {
            ServerBrowser.OnServersPolled += ServersPolled;
            this.gameArgs = gameArgs;
            this.launcherArgs = launcherArgs;

            ModSystem = new ModManager();
            ServerBrowser = new ServerBrowser();

            Settings.Load(Settings.GetLauncherSavePath());
            ModSystem.LoadSavedMods();
            List<(string, string)> filter = new List<(string, string)>();
            ServerBrowser.GetServers(filter);

        }

        public void Shutdown()
        {
            Settings.Save(Settings.GetLauncherSavePath(), ModSystem.GetEnabledMods());
            // If needed, ask other classes to stop doing things that might be too late to stop in the destructor
        }

        public class PlayOptions
        {
            public bool Force64Bit;
            public bool Force32Bit;
            public bool ForceBattlEye;
            public bool ForceRunViaSteam; // Linux: Force BA3L to launch Arma 3 via Steam instead of via Proton's executable
            public bool IncludeLaunchOptions; // Include launcher's launch options
            public bool PlayWithMods;
        };

        public bool Play() // Default play options
        {
            PlayOptions playOptions = new PlayOptions();
            playOptions.IncludeLaunchOptions = true;
            playOptions.PlayWithMods = true;
            return Play(playOptions);
        }

        public bool Play(PlayOptions playOptions)
        {
            playOptions.ForceBattlEye = Settings.PlayWithBattlEye;

            StringBuilder parameters = new StringBuilder();
            parameters.Append(GameArgsToString());
            if (playOptions.IncludeLaunchOptions)
            {
                parameters.Append(" ");
                parameters.Append(GenerateParameters());
            }


            if (playOptions.PlayWithMods)
            {
                string modLaunchParameters = ModSystem.GenerateModLaunchParameters();
                // There may be a maximum character limit that will be hit with too many mods' launch strings.
                // If this happens then look into using parameter files.
                // If there is a max limit, I haven't reached it yet.
                parameters.Append(@modLaunchParameters);
            }

            // TODO figure out what extra features are needed.
            if (Utilities.GetOS() == Utilities.OS.OS_WINDOWS)
            {
                return PlayWindows(playOptions, parameters);
            }
            else if (Utilities.GetOS() == Utilities.OS.OS_LINUX)
            {
                if (Utilities.IsProton() && !playOptions.ForceRunViaSteam)
                {
                    return PlayProton(playOptions, parameters);
                }
                else
                {
                    return PlayLinuxViaSteam(parameters);
                }
            }
            else if (Utilities.GetOS() == Utilities.OS.OS_OSX)
            {
                throw new Exceptions.UnsupportedOperatingSystemException("MacOS not supported yet");
            }
            else
            {
                throw new Exceptions.UnsupportedOperatingSystemException("Unknown Operating System!");
            }

        }

        private static bool PlayLinuxViaSteam(StringBuilder parameters)
        {
            Process.Start("/usr/bin/steam", "-applaunch 107410 " + @parameters + " -noLauncher");
            return true;
        }

        private static bool PlayWindows(PlayOptions playOptions, StringBuilder parameters)
        {
            string executable = GetWindowsExecutable(playOptions);
            string fullExePath = Path.Combine(Utilities.GetArma3Path(), executable);

            if (File.Exists(fullExePath))
            {
                Process.Start(fullExePath, parameters.ToString());
                return true;
            }

            return false; // Executable doesn't exist if we reach here
        }

        private static string GetWindowsExecutable(PlayOptions playOptions)
        {
            string executable;

            if (playOptions.ForceBattlEye)
            {
                executable = EXECUTABLE_BATTLEYE;
            }
            else if ((Environment.Is64BitOperatingSystem && !playOptions.Force32Bit) || playOptions.Force64Bit)
            {
                executable = EXECUTABLE_64BIT;
            }
            else
            {
                executable = EXECUTABLE_32BIT;
            }

            return executable;
        }

        private static bool PlayProton(PlayOptions playOptions, StringBuilder parameters)
        {
            // Require the user to input their Proton version manually.
            // Assume this method is only called from a Proton-compatible OS.
            // Run Proton if the executable is found
            if (Settings.ProtonPath == "")
            {
                throw new Exceptions.ProtonNotFoundException("Proton path not set!");
            }
            if (!File.Exists(Settings.ProtonPath))
            {
                throw new Exceptions.ProtonNotFoundException("Proton path not valid!");
            }

            // If we get here, Proton's executable should hopefully be valid.
            string executable = GetWindowsExecutable(playOptions);
            string fullExePath = Path.Combine(Utilities.GetArma3Path(), executable);
            if (File.Exists(fullExePath))
            {
                // Set working directory
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.WorkingDirectory = Utilities.GetArma3Path();
                startInfo.Arguments = "run \"" + fullExePath + "\" " + parameters;
                startInfo.FileName = Settings.ProtonPath;
                // Env vars
                string compatDataPath = Path.GetFullPath(Path.Combine(
                    Utilities.GetArma3Path(), "..", "..", "compatdata",
                    Utilities.ARMA3_APPID.ToString())
                );
                // Proton's way of setting Wine prefixes when calling the proton executable.
                startInfo.EnvironmentVariables["STEAM_COMPAT_DATA_PATH"] = compatDataPath;

                // Undocumented Proton env var required for steam-<AppId>.log in home dir if we have PROTON_LOG=1
                startInfo.EnvironmentVariables["SteamGameId"] = Utilities.ARMA3_APPID.ToString();

                // LD_PRELOAD the Steam Overlay
                // This probably won't work with Flatpak
                string oldPreload = Environment.GetEnvironmentVariable("LD_PRELOAD");
                string localAppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                string steamDir = Path.Combine(localAppData, "Steam");
                string newPreload = steamDir + "/ubuntu12_32/gameoverlayrenderer.so:" + steamDir + "/ubuntu12_64/gameoverlayrenderer.so";
                if (oldPreload != "")
                {
                    newPreload = oldPreload + ":" + newPreload;
                }
                startInfo.EnvironmentVariables["LD_PRELOAD"] = newPreload;

                startInfo.UseShellExecute = false;


                // TODO this will probably die if we close the launcher while Arma is running.
                Process p = Process.Start(startInfo);
                return true;
            }


            return false;
        }

        private string GameArgsToString()
        {
            return Utilities.ArgsToString(gameArgs.ToArray());
        }

        private string GenerateParameters()
        {
            StringBuilder parameters = new StringBuilder();

            // TODO go through more applicable settings and include them if needed.
            if (Settings.NoSplash)
                parameters.Append("-nosplash ");
            if (Settings.SkipIntro)
                parameters.Append("-skipIntro ");
            if (Settings.Windowed)
                parameters.Append("-window ");

            return parameters.ToString();
        }

        private void ServersPolled(List<ServerInfo> servers)
        {
            if (servers.Count > 0)
            {
                Server server = new Server(servers[0]);
                Console.WriteLine(servers.Count + " Servers Found");
                Console.WriteLine(server.ServerInfo.Name);
                server.GetRules();
            }
        }

    }
}
