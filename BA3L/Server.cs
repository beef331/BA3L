using System.Collections.Generic;
using System;
using Steamworks.Data;
using Steamworks.ServerList;
using System.Linq;
using System.Text;
using System.IO;
namespace BA3L
{
    /*
    Kart	0x1
    Marksmen	0x2
    Heli	0x4
    Curator	0x8
    Expansion	0x10
    Jets	0x20
    Orange	0x40
    Argo	0x80
    TacOps	0x100
    Tanks	0x200
    Contact	0x400
    Enoch	0x800
     */
    [Flags]
    public enum Expansions : int
    {
        Kart = 1,
        Marksmen = 2,
        Heli = 4,
        Curator = 8,
        Expansion = 16,
        Jets = 32,
        Orange = 64,
        Argo = 128,
        TacOps = 256,
        Tanks = 512,
        Contact = 1024,
        Enoch = 2048,
    }
    public class Server
    {
        public static Action<Server> OnRulesFetched;
        public ServerInfo ServerInfo { get; private set; }
        public Dictionary<string, string> Rules { get; private set; }
        public Mod[] ServerMods { get; private set; }
        public Expansions Expansions { get; private set; }
        public Server(ServerInfo server)
        {
            ServerInfo = server;
            GetRules();
        }
        private static byte[] RestoreBytes(byte[] oldBytes)
        {
            List<byte> newBytes = new List<byte>();
            // Using for loop instead of foreach because we need to look ahead
            for (int i = 0; i < oldBytes.Length; i++)
            {
                if (oldBytes[i] == 0x01)
                {
                    switch (oldBytes[i + 1])
                    {
                        case 0x01:
                            newBytes.Add(0x01);
                            i++;
                            break;
                        case 0x02:
                            newBytes.Add(0x00);
                            i++;
                            break;
                        case 0x03:
                            newBytes.Add(0xFF);
                            i++;
                            break;
                        default:
                            newBytes.Add(oldBytes[i]);
                            break;
                    }
                }
                else
                {
                    newBytes.Add(oldBytes[i]);
                }
            }

            return newBytes.ToArray();
        }
        /// <summary>
        /// Get rules and load values following https://community.bistudio.com/wiki/Arma_3_ServerBrowserProtocol3
        /// </summary>
        /// <returns></returns>
        public async void GetRules()
        {
            Rules = await ServerInfo.QueryRulesAsync();
            OnRulesFetched?.Invoke(this);
            int a = 0;
            foreach (KeyValuePair<string, string> kvp in Rules)
            {

                byte[] byteStream =  RestoreBytes(Encoding.UTF8.GetBytes(kvp.Value));
                Expansions = (Expansions)BitConverter.ToInt16(byteStream, 2);
                int expansionCount = Expansions.ToString().Split(',').Length;
                int currentIndex = 5 + 4 * expansionCount;
                ServerMods = new Mod[byteStream[currentIndex]];
                int currentMod = 0;
                using (BinaryWriter writer = new BinaryWriter(File.Open(Environment.CurrentDirectory + "/OutputTest" + a,FileMode.CreateNew)))
                {
                    writer.Write(byteStream);
                }
                a++;
                return;
            }
        }
    }
}